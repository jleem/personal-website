#!/usr/bin/env python

"""
graphgenerator.py
Description: 	
Feb 02, 2017
"""
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc

rc("font", **{"family": "sans-serif", "sans-serif": ["Helvetica"]})

N, M = np.random.normal(0,1,300), np.random.uniform(0,1,300)
plt.hexbin(N,M,gridsize=30,cmap = "PuBu")

plt.savefig("hexbin.png", dpi = 300)
