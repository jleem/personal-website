#!/usr/bin/env python

"""
parse_nba.py
Description: 	
Feb 02, 2017
"""
import numpy as np
import pandas as pd
from BeautifulSoup import BeautifulSoup
import sys

html = open(sys.argv[1])

soup = BeautifulSoup(html)
table = soup.find('table')
head = [ td.text for td in table.find('tr').findAll('td') ]

data = [ [ td.text for td in tr.findAll('td') ] for tr in table.findAll('tr')[1:] ][:-1]

df = pd.DataFrame(data = data, columns = head)
